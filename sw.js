const CACHE_NAME = 'service-worker-time-v1';

self.addEventListener('install', event => event.waitUntil(
    caches.open(CACHE_NAME)
        .then(cache => cache.addAll([
            './',
            './index.html',
            './js/init.js',
            './js/clock.js',
            './styles/style.css'
        ]))
        .catch(error => console.log('SW:cache.open:error', error))
));

self.addEventListener('fetch', event => event.respondWith(
    caches.match(event.request)
        .then(response => {
            if (response) {
                return response;
            }

            const fetchRequest = event.request.clone();

            return fetch(fetchRequest)
                .then(response => {
                    if(!response || response.status !== 200 || response.type !== 'basic') {
                        return response;
                    }

                    const responseToCache = response.clone();

                    caches.open(CACHE_NAME)
                        .then(cache => cache.put(event.request, responseToCache));

                    return response;
                })
                .catch(error => console.log('SW:fetch:error', error))
        })
        .catch(error => console.log('SW:cache.match:error', error))
));