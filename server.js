const http = require('http');
const fs = require('fs');
const path = require('path');
const mime = require('mime');

const CACHE = {};

const server = http.createServer((req, res) => {
    const filePath = req.url === '/' ? 'index.html' : req.url;
    const absPath = `./${filePath}`;

    serveStatic(res, CACHE, absPath);
});

server.listen(3000, () => {
    console.log('Server listening on port 3000.');
});

function send404(response) {
    response.writeHead(404, { 'Content-Type': 'text/plain' });
    response.write('Error 404: resource not found.');
    response.end();
}

function sendFile(response, filePath, fileContents) {
    response.writeHead(200, {
        'content-type': mime.lookup(path.basename(filePath))
    });
    response.end(fileContents);
}

function serveStatic(response, cache, absPath) {
    if (cache[absPath]) {
        sendFile(response, absPath, cache[absPath]);
    } else {
        fs.exists(absPath, exists => {
            if (!exists) {
                return send404(response);
            }

            fs.readFile(absPath, (error, data) => {
                if (error) {
                    return send404(response);
                }

                cache[absPath] = data;
                sendFile(response, absPath, data);
            });
        });
    }
}