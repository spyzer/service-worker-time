function clockInitialize() {
    let previousTimestamp = 0;

    const TRANSFORM_PREFIXES = [
        'webkitTransform',
        'mozTransform',
        'oTransform',
        'msTransform',
        'transform'
    ];

    const HoursArrow = document.getElementById('hoursArrow');
    const MinutesArrow = document.getElementById('minutesArrow');
    const SecondsArrow = document.getElementById('secondsArrow');

    const getHoursArrowDegree = now => {
        return now.getHours() % 12 * 5 + Math.floor(now.getMinutes() / 12)
    };

    const degrees2Radians = degrees => degrees * 6 - 180;

    const tick = () => {
        const now = new Date();

        if (now - previousTimestamp < 1000) {
            return window.requestAnimationFrame(tick);
        }

        const secondsRad = degrees2Radians(now.getSeconds());
        const minutesRad = degrees2Radians(now.getMinutes());
        const hoursRad = degrees2Radians(getHoursArrowDegree(now));

        TRANSFORM_PREFIXES.forEach(prefix => {
            HoursArrow.style[prefix] = `rotate(${hoursRad}deg)`;
            MinutesArrow.style[prefix] = `rotate(${minutesRad}deg)`;
            SecondsArrow.style[prefix] = `rotate(${secondsRad}deg)`;
        });

        previousTimestamp = now;

        window.requestAnimationFrame(tick);
    };

    window.requestAnimationFrame(tick);
}