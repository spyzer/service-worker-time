window.addEventListener('load', () => {
    serviceWorkerInitialization();

    clockInitialize();
}, {
    once: true
});

function serviceWorkerInitialization() {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('./sw.js')
            .then(registration => {
                console.log('Init:SW:info', 'SW registereg with scope: ', registration.scope);
            })
            .catch(error => {
                console.log('Init:SW:error', error);
            });
    }
}